require 'spec_helper'

describe 'soldel_restng::logger::appender', type: :define do
  let(:title) { 'test' }
  let :params do
    {
      appender_class: 'LoggerAppenderFile',
      layout_class: 'LoggerLayoutPattern'
    }
  end

  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', root_home: '/root')
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
      end
    end
  end
end

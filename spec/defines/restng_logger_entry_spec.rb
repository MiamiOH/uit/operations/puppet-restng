require 'spec_helper'

describe 'soldel_restng::logger::entry', type: :define do
  let(:title) { 'test' }

  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', root_home: '/root')
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
      end
    end
  end
end

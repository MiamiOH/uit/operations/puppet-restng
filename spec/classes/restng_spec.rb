require 'spec_helper'

describe 'soldel_restng', type: :class do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', root_home: '/root')
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('soldel_restng::install') }
        it { is_expected.to contain_class('soldel_restng::logger') }
        it { is_expected.to contain_class('soldel_restng::env') }
        it { is_expected.to contain_class('soldel_restng::datasources') }
        it { is_expected.to contain_class('soldel_restng::autoloaders') }
        it { is_expected.to contain_class('soldel_restng::providers') }
        it { is_expected.to contain_class('soldel_restng::services') }
      end

      context 'without datasources' do
        let(:params) do
          {
            manage_datasources: false
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.not_to contain_class('soldel_restng::datasources') }
      end

      context 'without autoloaders' do
        let(:params) do
          {
            manage_autoloaders: false
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.not_to contain_class('soldel_restng::autoloaders') }
      end

      context 'without providers' do
        let(:params) do
          {
            manage_providers: false
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.not_to contain_class('soldel_restng::providers') }
      end

      context 'without services' do
        let(:params) do
          {
            manage_services: false
          }
        end

        it { is_expected.to compile.with_all_deps }
        it { is_expected.not_to contain_class('soldel_restng::services') }
      end
    end
  end
end

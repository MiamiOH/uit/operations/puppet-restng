# Class: soldel_restng::env
#
# Private class used only by soldel_restng
# This class creates the env file
#

class soldel_restng::env (
  $restng_env          = 'development',
  Hash $env_entries    = {},
) {

  include 'soldel_restng'

  file { "${soldel_restng::restng_home}/.env":
    ensure  => file,
    owner   => $soldel_restng::restng_user,
    group   => $soldel_restng::restng_group,
    mode    => '0440',
    content => $env_entries.merge({ 'RESTNG_ENVIRONMENT' => $restng_env }).join_keys_to_values('=').join("\n"),
  }
}

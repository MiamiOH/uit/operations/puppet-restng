# Class: soldel_restng::services
#
# Private class used only by soldel_restng
# This class creates the services config file
#

class soldel_restng::services (
  Hash $services        = {},
) {

  include 'soldel_restng'

  file { "${soldel_restng::restng_home}/config/services.yaml":
    ensure  => file,
    owner   => $soldel_restng::restng_user,
    group   => $soldel_restng::restng_group,
    mode    => '0644',
    content => { services => $services }.to_yaml,
  }

}

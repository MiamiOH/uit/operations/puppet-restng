# Class: soldel_restng::install
#
# Private class used only by soldel_restng
# This class handle RESTng installation tasks
#

class soldel_restng::install {

  include 'soldel_restng'

  package { 'miamioh-restng':
    ensure => latest,
  }

  if $soldel_restng::addon_package {
    package { $soldel_restng::addon_package:
      ensure => latest,
    }
  }

  if $soldel_restng::expose_www {
    file { '/var/www/html/api':
      ensure  => link,
      owner   => $soldel_restng::restng_user,
      group   => $soldel_restng::restng_group,
      target  => "${soldel_restng::restng_home}/www",
      require => Package['miamioh-restng'],
    }
  }

}

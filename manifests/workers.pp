# Class: soldel_restng::workers
#
# Private class used only by soldel_restng
# This class creates the worker unit files
#

class soldel_restng::workers (
  Hash $workers = {},
) {

  include 'soldel_restng'

  create_resources('soldel_restng::worker', $workers,
    {
      restng_home  => $soldel_restng::restng_home,
      restng_user  => $soldel_restng::restng_user,
      restng_group => $soldel_restng::restng_group,
    }
  )
}

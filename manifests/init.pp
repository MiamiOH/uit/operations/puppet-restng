# Class: soldel_restng
#
# This class installs and configures RESTng

class soldel_restng (
  $restng_home                 = $soldel_restng::params::restng_home,
  $restng_user                 = $soldel_restng::params::restng_user,
  $restng_group                = $soldel_restng::params::restng_group,
  $log_dir                     = $soldel_restng::params::log_dir,
  $log_owner                   = $soldel_restng::params::log_owner,
  $log_group                   = $soldel_restng::params::log_group,
  $expose_www                  = $soldel_restng::params::expose_www,
  $composer_home               = $soldel_restng::params::composer_home,

  $restng_env                  = undef,
  $env_entries                 = undef,

  $manage_datasources          = true,
  Hash $datasources            = {},
  $manage_autoloaders          = true,
  Array $autoloaders           = [],
  $manage_providers            = true,
  Hash $providers              = {},
  $manage_services             = true,
  Hash $services               = {},
  $manage_workers              = true,
  Hash $workers                = {},
  $manage_cron_entries         = true,
  Hash $cron_entries           = {},

  $addon_package               = undef,
  $addon_install_path          = undef,

  Array $root_logger_appenders = $soldel_restng::params::root_logger_appenders,
  Enum['debug', 'info', 'warn', 'error', 'fatal'] $root_logger_level = $soldel_restng::params::root_logger_level,
  Hash $loggers                = {},
  Hash $appenders              = {},

  $composer_environment = undef,

) inherits soldel_restng::params {

  # Create the main api log appender. Other appenders can be created,
  # but this one always exists.
  soldel_restng::logger::appender { 'apiLogFile':
    appender_class => 'LoggerAppenderFile',
    layout_class   => 'LoggerLayoutPattern',
    layout_params  => {
      conversionPattern => '%level %date [%logger] %message%newline%ex',
    },
    params         => {
      file => "${log_dir}/api.log",
    },
  }

  class { 'soldel_restng::install': }
  contain 'soldel_restng::install'

  class { 'soldel_restng::logger':
    log_owner             => $log_owner,
    log_group             => $log_group,
    log_dir               => $log_dir,
    root_logger_appenders => $root_logger_appenders,
    root_logger_level     => $root_logger_level,
    loggers               => $loggers,
    appenders             => $appenders,
    require               => Class['soldel_restng::install'],
  }
  contain 'soldel_restng::logger'

  class { 'soldel_restng::env':
    restng_env  => $restng_env,
    env_entries => $env_entries,
    require     => Class['soldel_restng::install'],
  }
  contain 'soldel_restng::env'

  if $manage_datasources {
    class { 'soldel_restng::datasources':
      datasources => $datasources,
      require     => Class['soldel_restng::install'],
    }
    contain 'soldel_restng::datasources'
  }

  if $manage_autoloaders {
    class { 'soldel_restng::autoloaders':
      autoloaders        => $autoloaders,
      addon_install_path => $addon_install_path,
      require            => Class['soldel_restng::install'],
    }
    contain 'soldel_restng::autoloaders'
  }

  if $manage_providers {
    class { 'soldel_restng::providers':
      providers          => $providers,
      addon_install_path => $addon_install_path,
      require            => Class['soldel_restng::install'],
    }
    contain 'soldel_restng::providers'
  }

  if $manage_services {
    class { 'soldel_restng::services':
      services => $services,
      require  => Class['soldel_restng::install'],
    }
    contain 'soldel_restng::services'
  }

  if $manage_workers {
    class { 'soldel_restng::workers':
      workers => $workers,
      require => Class['soldel_restng::install'],
    }
    contain 'soldel_restng::workers'
  }

  if $manage_cron_entries {
    class { 'soldel_restng::cron_entries':
      cron_entries => $cron_entries,
      require      => Class['soldel_restng::install'],
    }
    contain 'soldel_restng::cron_entries'
  }

}

# Class: soldel_restng::autoloaders
# 
# Private class used only by soldel_restng
# This class creates the autoloaders file
#

class soldel_restng::autoloaders (
  Array $autoloaders  = [],
  $addon_install_path = undef,
) {

  include 'soldel_restng'

  $all_autoloaders = $addon_install_path ? {
    undef   => $autoloaders,
    default => concat($autoloaders, "${addon_install_path}/vendor/autoload.php"),
  }

  file { "${soldel_restng::restng_home}/config/autoloaders.yaml":
    ensure  => file,
    owner   => $soldel_restng::restng_user,
    group   => $soldel_restng::restng_group,
    mode    => '0644',
    content => { 'autoload' => $all_autoloaders }.to_yaml,
  }

}

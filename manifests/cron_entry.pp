# Define: soldel_restng::cron_entry
#
# Private class used only by soldel_restng
# Defines a cron_entry process
#

define soldel_restng::cron_entry (
  $ensure      = present,
  $restng_home = undef,
  $restng_user = undef,
  $environment = undef,
  $weekday     = '*',
  $hour        = '*',
  $minute      = '*',
  $command     = undef,
) {

  cron { $name:
    ensure      => $ensure,
    command     => "/usr/bin/php ${restng_home}/console ${command}",
    environment => $environment,
    user        => $restng_user,
    weekday     => $weekday,
    hour        => $hour,
    minute      => $minute,
    require     => Class['soldel_restng::install'],
  }

}

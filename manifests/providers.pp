# Class: soldel_restng::providers
#
# Private class used only by soldel_restng
# This class creates the providers file
#

class soldel_restng::providers (
  Hash $providers     = {},
  $addon_install_path = undef
) {

  include 'soldel_restng'

  $resources = $providers['resources'] ? {
    Array   => $providers['resources'],
    default => [],
  }

  $all_resources = $addon_install_path ? {
    undef   => $resources,
    default => concat($resources, "${addon_install_path}/resources.php"),
  }

  file { "${soldel_restng::restng_home}/config/providers.yaml":
    ensure  => file,
    owner   => $soldel_restng::restng_user,
    group   => $soldel_restng::restng_group,
    mode    => '0644',
    content => { 'resources' => $all_resources }.to_yaml,
  }

}

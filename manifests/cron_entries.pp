# Class: soldel_restng::cron_entries
#
# Private class used only by soldel_restng
# This class creates cron entries
#

class soldel_restng::cron_entries (
  Hash $cron_entries = {},
) {

  include 'soldel_restng'

  create_resources('soldel_restng::cron_entry', $cron_entries,
    {
      restng_home  => $soldel_restng::restng_home,
      restng_user  => $soldel_restng::restng_user,
    }
  )
}

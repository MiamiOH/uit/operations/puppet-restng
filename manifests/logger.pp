# Class: soldel_restng::logger
#
# Private class used only by soldel_restng
# This class creates the RESTng Log4PHP config file
#

class soldel_restng::logger (
  $log_owner                   = 'root',
  $log_group                   = 'root',
  $log_dir                     = '/var/log/restng',
  Array $root_logger_appenders = $soldel_restng::params::root_logger_appenders,
  Enum['debug', 'info', 'warn', 'error', 'fatal'] $root_logger_level = $soldel_restng::params::root_logger_level,
  Hash $loggers                = {},
  Hash $appenders              = {},
) {

  include 'soldel_restng'

  file { $log_dir:
    ensure => directory,
    mode   => '0775',
    owner  => $log_owner,
    group  => $log_group,
  }

  if str2bool($::selinux) {
    selinux::fcontext { 'set-httpd-log-context-restng':
      seltype  => 'httpd_log_t',
      pathspec => "${log_dir}(/.*)?",
    } ~> selinux::exec_restorecon { $log_dir: }
  }

  # Create the log configuration file.
  concat { 'logger':
    path    => "${soldel_restng::restng_home}/config/log4php.conf.php",
    owner   => $soldel_restng::restng_user,
    group   => $soldel_restng::restng_group,
    mode    => '0644',
    require => File[$log_dir],
  }

  concat::fragment { 'logger_head':
    target  => 'logger',
    order   => '01',
    content => template("${module_name}/logger/log4php_head.erb"),
  }

  concat::fragment { 'root_logger':
    target  => 'logger',
    order   => '10',
    content => template("${module_name}/logger/root_logger.erb"),
  }

  concat::fragment { 'logger_start':
    target  => 'logger',
    order   => '20',
    content => template("${module_name}/logger/logger_start.erb"),
  }

  concat::fragment { 'logger_end':
    target  => 'logger',
    order   => '29',
    content => template("${module_name}/logger/logger_end.erb"),
  }

  concat::fragment { 'appender_start':
    target  => 'logger',
    order   => '40',
    content => template("${module_name}/logger/appender_start.erb"),
  }

  concat::fragment { 'appender_end':
    target  => 'logger',
    order   => '49',
    content => template("${module_name}/logger/appender_end.erb"),
  }

  concat::fragment { 'logger_foot':
    target  => 'logger',
    order   => '99',
    content => template("${module_name}/logger/log4php_foot.erb"),
  }

  create_resources('soldel_restng::logger::entry', $loggers)
  create_resources('soldel_restng::logger::appender', $appenders)

}

# Class: soldel_restng::params
#
# This class manages parameters for RESTng

class soldel_restng::params {
  $restng_home           = '/var/www/restng'
  $restng_user           = 'root'
  $restng_group          = 'root'
  $log_dir               = '/var/log/restng'
  $log_owner             = 'root'
  $log_group             = 'root'
  $expose_www            = true
  $composer_home         = '/root'

  $cache_data_dir        = 'restng'

  $root_logger_appenders = ['apiLogFile']
  $root_logger_level     = 'warn'

}

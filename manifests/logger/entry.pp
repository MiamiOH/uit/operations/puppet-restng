# Define: soldel_restng::logger::entry
#
# Creates a logger entry in the Log4PHP configuration
#

define soldel_restng::logger::entry (
  Array $appenders = [],
  Enum['debug', 'info', 'warn', 'error', 'fatal'] $level = 'info',
){

  concat::fragment { "logger_${name}":
    target  => 'logger',
    order   => '25',
    content => template("${module_name}/logger/logger.erb"),
  }

}

# Define: soldel_restng::logger::appender
#
# Creates a logger appender in the Log4PHP configuration
#

define soldel_restng::logger::appender (
  $appender_class,
  $layout_class,
  Hash $layout_params = {},
  Hash $params        = {},
){

  concat::fragment { "appender_${name}":
    target  => 'logger',
    order   => '45',
    content => template("${module_name}/logger/appender.erb"),
  }

}

# Define: soldel_restng::worker
#
# Private class used only by soldel_restng
# Defines a worker process
#

define soldel_restng::worker (
  $ensure             = present,
  $restng_home        = undef,
  $restng_user        = undef,
  $restng_group       = undef,
  $worker_environment = undef,
  $command            = undef,
) {

  if ($ensure == present) {
    $unit_ensure    = present
    $service_ensure = running
    $service_enable = true
  } else {
    $unit_ensure    = absent
    $service_ensure = stopped
    $service_enable = false
  }

  $worker_name = "restng-${name}"

  $worker_environment_vars = $worker_environment ? {
    String  => [ $worker_environment ],
    Array   => $worker_environment,
    undef   => [],
    default => fail('soldel_restng::worker - Unsupported worker environment value'),
  }

  if $::service_provider == 'systemd' {
    systemd::unit_file { "${worker_name}.service":
      ensure  => $unit_ensure,
      content => template("${module_name}/worker.service.erb"),
      notify  => Service[$worker_name],
    }

    service { $worker_name:
      ensure     => $service_ensure,
      enable     => $service_enable,
      hasstatus  => true,
      hasrestart => true,
      subscribe  => Package['miamioh-restng'],
    }

  }

}

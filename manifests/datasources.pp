# Class: soldel_restng::datasources
#
# Private class used only by soldel_restng
# This class creates the RESTng datasources file
#

class soldel_restng::datasources (
  Hash $datasources = {},
) {

  include 'soldel_restng'

  # Ensure that the expected keys all exist, even if undefined
  $cleaned_datasources = $datasources.reduce({}) |$c, $d| {
    $clean = {
      type         => $d[1]['type'],
      user         => $d[1]['user'],
      password     => $d[1]['password'],
      host         => $d[1]['host'],
      database     => $d[1]['database'],
      port         => $d[1]['port'],
      connect_type => $d[1]['connect_type'],
    }

    $c + { $d[0] => $clean }
  }

  file { "${soldel_restng::restng_home}/config/datasources.yaml":
    ensure  => file,
    owner   => $soldel_restng::restng_user,
    group   => $soldel_restng::restng_group,
    mode    => '0600',
    content => $cleaned_datasources.to_yaml,
  }

}
